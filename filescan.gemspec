Gem::Specification.new do |s|
	s.name             = 'filescan'
	s.version          = '2.3.4'
	s.date             = '2023-09-08'
	s.summary          = "Class to recursively scan through files from a given directory."
	s.description      = "Class to recursively scan through files from a given directory."
	s.authors          = ["Alan Davies"]
	s.email            = 'alan n davies at gmail com'
	s.files            = ['lib/filescan.rb', 'History.txt', 'README.rdoc']
	s.extra_rdoc_files = ['README.rdoc']
	s.homepage         = 'https://gitlab.com/cs96and/filescan'
	s.metadata         = { "source_code_uri" => "https://gitlab.com/cs96and/filescan" }
end
