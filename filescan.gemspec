Gem::Specification.new do |s|
	s.name             = 'filescan'
	s.version          = '2.3.2'
	s.date             = '2011-11-09'
	s.summary          = "Class to recursively scan through files from a given directory."
	s.description      = "Class to recursively scan through files from a given directory."
	s.authors          = ["Alan Davies"]
	s.email            = 'alan n davies at gmail com'
	s.files            = ['lib/filescan.rb', 'History.txt', 'README.rdoc']
	s.extra_rdoc_files = ['README.rdoc']
	s.homepage         = 'http://rubygems.org/gems/filescan'
end
