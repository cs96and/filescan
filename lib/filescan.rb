# == Synopsis
#
# Class to recursively scan through files from a given directory.
#
# == Copyright
#
# Copyright (C) 2003, 2008, 2009, 2011 by Alan Davies.
#
# == License
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.

# Class to recursively scan through files from a given directory.
class Filescan
	include Enumerable

	VERSION = "2.3.2"

	# Default root directory for calls to each.
	attr_accessor :startDir
	# Whether to recurse into subdirectories (boolean).
	attr_accessor :recursive
	# Whether to output directory names (boolean).
	attr_accessor :verbose
	# Array of exceptions to allow (defaults to [Errno::EACCES])
	# * If one of these exceptions is thrown then an error is shown on stderr, but processing continues.
	# * If any other exception is thrown then processing stops and the exception is passed back to the user.
	attr_accessor :allowedExceptions

	#	Create a new Filescan Object.
	#
	# :call-seq:
	#  new(dir, recusive, verbose) -> Filescan
	#  new(dir, recusive, verbose) { |filescan| ... } -> nil
	#
	def initialize(dir='.', recursive=true, verbose=false, allowedExceptions=[Errno::EACCES]) # :yields: filescan
		@startDir = dir
		@recursive = recursive
		@verbose = verbose
		@allowedExceptions = allowedExceptions
		
		yield self if block_given?
	end

	# Execute a block for every entry in a directory.
	def each(startDir=@startDir, &block) # :yields: fullPath
		puts startDir if @verbose
		begin
			Dir.open(startDir) do |dir|
				dir.each do |entry|
					next if entry == '.' or entry == '..'
					yield(fullPath = File.expand_path(entry, startDir))
					self.each(fullPath, &block) if @recursive and File.directory?(fullPath)
				end
			end
		rescue Exception => e
			if allowedExceptions.any? { |allowed| e.is_a?(allowed) }
				$stderr.puts e
				return
			else
				raise
			end
		end
	end # def each

	# Executes a block for each directory name or file name, where any part of the path matches a pattern.
	# If you do not specify a pattern, then it is equivilent to calling 'each'.
	# If no block is given, then it returns an array of matching results.
	#
	# :call-seq:
	#  each_pathname(pattern=//) -> Array
	#  each_pathname(pattern=//) { |pathName| ... } -> nil
	#
	def each_pathname(pattern=//) # :yields: pathName
		if block_given?
			self.each { |pathName| yield pathName if pathName.match(pattern) }
		else
			a = []
			self.each_pathname(pattern) { |pathName| a << pathName }
			return a
		end
	end

	# Executes a block for each directory name.
	# If no block is given, then it returns an array of matching results.
	#
	# :call-seq:
	#  each_dirname(pattern=//) -> Array
	#  each_dirname(pattern=//) { |dirName| ... } -> nil
	#
	def each_dirname(pattern=//) # :yields: dirName
		if block_given?
			self.each { |dirName| yield dirName if File.directory?(dirName) and File.basename(dirName).match(pattern) }
		else
			a = []
			self.each_dirname(pattern) { |dirName| a << dirName }
			return a
		end
	end

	# Executes a block for each directory.
	def each_dir(pattern=//)
		each_dirname(pattern) do |dirName|
			Dir.open(dirName) { |dirHandle| yield(dirHandle, dirName) }
		end
	end

	# Execute a block for each filename.
	# If no block is given, then it returns an array of matching results.
	#
	# :call-seq:
	#  each_filename(pattern=//) -> array
	#  each_filename(pattern=//) { |fileName| ... } -> nil
	#
	def each_filename(pattern=//) # :yields: fileName
		if block_given?
			self.each { |fileName| yield fileName if File.file?(fileName) and File.basename(fileName).match(pattern) }
		else
			a = []
			self.each_filename(pattern) { |fileName| a << fileName }
			return a
		end
	end

	# Execute a block for each file.
	def each_file(mode='r', pattern=//)
		self.each_filename(pattern) do |filename|
			File.open(filename, mode) { |file| yield(file, filename) }
		end
	end

	# Executes a block for each line of each file.
	# If the files are opened in writable mode, then the block must return the number of changes that were made to the line.
	# Files are not written back to the disk if no lines are altered.
	#
	# :call-seq:
	#  each_line(writable=false, pattern=//) -> array
	#  each_line(writable=false, pattern=//) { |file, filename| ... } -> nil
	#
	def each_line(writable=false, pattern=//)
		self.each_file(writable ? 'r+' : 'r', pattern) do |file, filename|
			fileChanges = 0

			# Yield the block for each line in the file
			file.each_line do |line|
				lineChanges = yield(line, filename)
				fileChanges += lineChanges if writable
			end

			# Write the file back to disk
			if writable and (fileChanges > 0)
				puts "#{filename} - #{fileChanges} changes made" if @verbose
				file.rewind
				file.write(text)
				file.truncate(file.pos)
			end
		end # self.eachfile
	end # def each_line

end # class Filescan
